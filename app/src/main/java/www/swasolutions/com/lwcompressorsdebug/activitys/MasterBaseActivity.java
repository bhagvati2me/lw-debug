package www.swasolutions.com.lwcompressorsdebug.activitys;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.lang.ref.WeakReference;

import www.swasolutions.com.lwcompressorsdebug.Bluetooth.DeviceConnector;
import www.swasolutions.com.lwcompressorsdebug.Bluetooth.DeviceData;
import www.swasolutions.com.lwcompressorsdebug.Bluetooth.DeviceListActivity;
import www.swasolutions.com.lwcompressorsdebug.Bluetooth.L;
import www.swasolutions.com.lwcompressorsdebug.Bluetooth.Utils;
import www.swasolutions.com.lwcompressorsdebug.Database;
import www.swasolutions.com.lwcompressorsdebug.R;

/**
 * Created by bhagvati on 18/11/17.
 */

public class MasterBaseActivity extends Activity {

    SharedPreferences pref;

    // ==========================BLUETOOTH===============================
    static final int REQUEST_CONNECT_DEVICE = 1;
    static final int REQUEST_ENABLE_BT = 2;
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    BluetoothAdapter btAdapter;
    public static final String SAVED_PENDING_REQUEST_ENABLE_BT = "PENDING_REQUEST_ENABLE_BT";
    boolean pendingRequestEnableBt = false;
    boolean showlist = false;
    public static DeviceConnector connector;
    public static BluetoothResponseHandler mHandler;
    public static String deviceName = "";
    public static String CONNECTION = "";
    public static String MSG_NOT_CONNECTED;
    public static String MSG_CONNECTING;
    public static String MSG_CONNECTED;

    // ==========================BLUETOOTH===============================


    public void toggleHideyBar() {
        int uiOptions = this.getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }
        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
    }

    void hideSystemUI() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_OVERSCAN);
        }*/
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        pref = PreferenceManager.getDefaultSharedPreferences(this);

        // ===========================================BLUETOOTH===============================

        MSG_NOT_CONNECTED = getString(R.string.msg_not_connected);
        MSG_CONNECTING = getString(R.string.msg_connecting);
        MSG_CONNECTED = getString(R.string.msg_connected);

        if (savedInstanceState != null) {
            pendingRequestEnableBt = savedInstanceState.getBoolean(SAVED_PENDING_REQUEST_ENABLE_BT);
        }
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter == null) {
            final String no_bluetooth = getString(R.string.no_bt_support);
            showAlertDialog(no_bluetooth);
            Utils.log(no_bluetooth);
        }

        if (isConnected()) {
            CONNECTION = MSG_CONNECTED;
            setmodel();

        } else {
            CONNECTION = MSG_NOT_CONNECTED;
            setmodel();

        }

        if (isAdapterReady() && (connector == null)) {
            if (!Constant.isdebug) {
                //  startDeviceListActivity();
                setautoconncet();
            }
        } else {
            L.e(deviceName);
            //  setmodel();
        }

       /* String data = "#500000210 ANS0+     0,0  GradC ";
        sendCommand(data);*/
        // ===========================================BLUETOOTH===============================

    }


    // ====================================BLUETOOTh=================================

    public void setmodel() {

    }

    void showAlertDialog(String message) {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.app_name));
        alertDialogBuilder.setMessage(message);
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    boolean isAdapterReady() {
        return (btAdapter != null) && (btAdapter.isEnabled());
    }

    void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
        L.e(deviceName);
    }

    public void sendCommand(String data,int VaNr) {

        if (isConnected()) {
			//added by CAC
			// Only needed:
			// "TA01_1199_ST01_VAxx\r\n" 
			// + 1-32x "#000.....\r\n" 
			// + "CRC\r\n" 
            String Data1 = "TA01_1199_ST01_VA";

            String datas = data.trim();

            //added by CAC
            while (datas.length()<32)
                datas = datas + " ";
            //added by CAC
            datas = datas + "\r\n";


            if (VaNr<10)
                Data1 = Data1 + "0";
            Data1 = Data1 + Integer.toString(VaNr)+ "\r\n";

            datas = Data1 + datas;
            L.e("Origanl " + datas);
            datas = datas + crc(datas.getBytes());
            L.e("Final send data " + datas);
            connector.write(datas.getBytes());
        }

    }

	//added by CAC
    public void sendVaNr(int VaNr) {

        if (isConnected()) {
            String datas = "?VA";

            if (VaNr<10)
                datas = datas + "0";
            datas = datas + Integer.toString(VaNr);

            datas = datas + "\r\n";
            L.e("Origanl " + datas);

            connector.write(datas.getBytes());
        }

    }


    public static int CRC16(int crc, int data) {
        int Poly16 = 0xA001;
        int LSB, i;
        crc = ((crc ^ data) | 0xFF00) & (crc | 0x00FF);
        for (i = 0; i < 8; i++) {
            LSB = (crc & 0x0001);
            crc = crc / 2;
            L.e("LSB " + LSB);
            if (LSB == 1)
                crc = crc ^ Poly16;
        }
        return (crc);

    }

    public static String crc(byte[] args) {
        int crc = 0xFFFF;
        byte[] bytes = args;
        for (byte b : bytes) {
            crc = CRC16(crc, b);
        }
        String le = Integer.toHexString(crc);

        //added by CAC
        while (le.length()<4)
            le = "0" + le;
        le.toUpperCase();
        //added by CAC

        le = le + "\r\n";
        return le;
    }

    /* public static String crc(byte[] args) {
         int crc = 0xFFFF;          // initial value
         int polynomial = 0xA001;   // 0001 0000 0010 0001  (0, 5, 12)

         // byte[] testBytes = "123456789".getBytes("ASCII");

         byte[] bytes = args;

         for (byte b : bytes) {
             for (int i = 0; i < 8; i++) {
                 boolean bit = ((b >> (7 - i) & 1) == 1);
                 boolean c15 = ((crc >> 15 & 1) == 1);
                 crc <<= 1;
                 if (c15 ^ bit) crc ^= polynomial;
             }
         }

         crc &= 0xffff;
         String le = Integer.toHexString(crc);
         le = le + " \\r\\n";

         L.e("CRC16 = " + le);
         return le;
     }
 */
    private void setupConnector(BluetoothDevice connectedDevice) {
        stopConnection();
        try {
            String emptyName = getString(R.string.empty_device_name);
            DeviceData data = new DeviceData(connectedDevice, emptyName);
            connector = new DeviceConnector(data, mHandler);
            connector.connect();
        } catch (IllegalArgumentException e) {
            Utils.log("setupConnector failed: " + e.getMessage());
        }
    }

    public static void stopConnection() {
        if (connector != null) {
            connector.stop();
            connector = null;
            deviceName = null;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SAVED_PENDING_REQUEST_ENABLE_BT, pendingRequestEnableBt);
    }


    private class BluetoothResponseHandler extends Handler {
        private WeakReference<MasterBaseActivity> mActivity;

        public BluetoothResponseHandler(MasterBaseActivity activity) {
            mActivity = new WeakReference<MasterBaseActivity>(activity);
        }

        public void setTarget(MasterBaseActivity target) {
            mActivity.clear();
            mActivity = new WeakReference<MasterBaseActivity>(target);
        }

        @Override
        public void handleMessage(Message msg) {
            MasterBaseActivity activity = mActivity.get();
            if (activity != null) {
                switch (msg.what) {
                    case MESSAGE_STATE_CHANGE:

                        switch (msg.arg1) {
                            case DeviceConnector.STATE_CONNECTED:
                                setstatus(MSG_CONNECTED);
                                CONNECTION = MSG_CONNECTED;
                                L.e(MSG_CONNECTED);
                                setmodel();
                                break;
                            case DeviceConnector.STATE_CONNECTING:
                                setstatus(MSG_CONNECTING);
                                CONNECTION = MSG_CONNECTING;
                                L.e(MSG_CONNECTING);
                                setmodel();
                                Constant.setdevice(getApplicationContext(), DeviceConnector.DEVICEADDRESS);
                                break;
                            case DeviceConnector.STATE_NONE:
                                L.e("trying->" + trying);
                                if (trying < 5) {
                                    trying++;
                                    setautoconncet();
                                    setstatus(MSG_NOT_CONNECTED);
                                    CONNECTION = MSG_NOT_CONNECTED;
                                    L.e(MSG_NOT_CONNECTED);
                                    setmodel();
                                }


                                break;
                        }
                        break;

                    case MESSAGE_READ:

                        final String readMessage = (String) msg.obj;
                        // Log.e("Read", readMessage);
                        if (readMessage != null) {
                            setdata(readMessage);
                        }
                        break;

                    case MESSAGE_DEVICE_NAME:
                        activity.setDeviceName((String) msg.obj);
                        setmodel();
                        break;

                    case MESSAGE_WRITE:
                        // stub
                        break;

                    case MESSAGE_TOAST:
                        // stub
                        break;
                }
            }
        }
    }

    int trying = 0;

    public void setstatus(String connected) {

    }

    public void setdata(String readMessage) {
        Intent intent = new Intent("send");
        intent.putExtra("msg", readMessage);
        sendBroadcast(intent);
    }


    @Override
    public void onStart() {
        super.onStart();
        checkble();
    }

    private void checkble() {
        if (btAdapter == null)
            return;

        if (!btAdapter.isEnabled() && !pendingRequestEnableBt) {
            pendingRequestEnableBt = true;
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
    }

    public static boolean isConnected() {
        return (connector != null) && (connector.getState() == DeviceConnector.STATE_CONNECTED);
    }

    public void startDeviceListActivity() {
        checkble();
        stopConnection();
        Intent serverIntent = new Intent(this, DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
    }


    public void setautoconncet() {
        if (!pref.getString(Constant.address, "").isEmpty()) {
            BluetoothDevice device = btAdapter.getRemoteDevice(pref.getString(Constant.address, ""));
            if (isAdapterReady() && (connector == null))
                if (mHandler == null)
                    mHandler = new BluetoothResponseHandler(this);
                else
                    mHandler.setTarget(this);
            setupConnector(device);
        } else {
            startDeviceListActivity();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    showlist = true;
                    String address = data.getStringExtra(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    BluetoothDevice device = btAdapter.getRemoteDevice(address);
                    if (isAdapterReady() && (connector == null))
                        if (mHandler == null)
                            mHandler = new BluetoothResponseHandler(this);
                        else
                            mHandler.setTarget(this);
                    setupConnector(device);

                }
                break;
            case REQUEST_ENABLE_BT:
                pendingRequestEnableBt = false;
                if (resultCode != Activity.RESULT_OK) {
                    Utils.log("BT not enabled");
                } else {
                    if (isAdapterReady()) {
                        if (isConnected())
                            stopConnection();
                        else
                            startDeviceListActivity();
                    } else {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                    }
                }
                break;
        }
    }
    // ====================================BLUETOOTh=================================


    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(master_receiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(master_receiver, new IntentFilter("send"));
    }

    private BroadcastReceiver master_receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context c, Intent intent) {

            String readMessage = intent.getStringExtra("msg");
            // L.d(readMessage);
            if (readMessage.contains("#")) {
                String[] separated = readMessage.split("\n");
                for (int i = 0; i < separated.length; i++) {
                    if (separated[i].contains("bar") || separated[i].contains("GradC") || separated[i].contains("mg/m3") ||
                            separated[i].contains("ppm") || separated[i].contains("%") || separated[i].contains("l/min")) {

                        if (separated[i].contains("#")) {
                            // String cutline = "#075001010 STA3+     0,0  GradC ".substring(11);
                            try {
                                if (separated[i].length() > 16) {
                                    String cutline = separated[i].substring(11);
                                    String code = cutline.substring(0, 5).replace("+", "");
                                    String name = Constant.getNamefromcode(code);
                                    if (!name.equals("")) {
                                        String meaurement = cutline.substring(5).trim();
                                        meaurement = Constant.getrefine(meaurement).replace(code, "").replace(",", ".").
                                                replace("bar", "").replace("GradC", "").replace("mg/m3", "").
                                                replace("ppm", "").replace("%", "").replace("l/min", "");
                                        Database.createGraph(code, meaurement, "");
                                        Database.deletereords();
                                    }

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }

                    }
                }
            }

        }
    };

}

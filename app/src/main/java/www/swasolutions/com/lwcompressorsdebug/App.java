package www.swasolutions.com.lwcompressorsdebug;

import android.app.Application;

import io.github.skyhacker2.sqliteonweb.SQLiteOnWeb;

/**
 * Created by bhagvati on 9/12/17.
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Database.init(getApplicationContext());
        SQLiteOnWeb.init(this).start();



    }
}

package www.swasolutions.com.lwcompressorsdebug.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

/**
 * Created by bhagvati on 25/11/17.
 */

public class CustomeButton extends android.support.v7.widget.AppCompatButton {
    public CustomeButton(Context context) {
        super(context);
        init(null);
    }

    public CustomeButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomeButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }


    private void init(AttributeSet attrs) {
        // Just Change your font name
        Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/HUM777N.TTF");
        setTypeface(myTypeface);

    }
}

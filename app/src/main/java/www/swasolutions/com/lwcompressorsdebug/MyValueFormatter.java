package www.swasolutions.com.lwcompressorsdebug;

import com.github.mikephil.charting.utils.ValueFormatter;

/**
 * Created by AndroidDev3 on 9/17/2016.
 */
public class MyValueFormatter implements ValueFormatter {
    @Override
    public String getFormattedValue(float value) {

        int tempValue = Math.round(value);
        return tempValue + "";
    }
}

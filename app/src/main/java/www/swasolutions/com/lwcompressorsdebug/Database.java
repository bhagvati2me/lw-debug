package www.swasolutions.com.lwcompressorsdebug;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import www.swasolutions.com.lwcompressorsdebug.activitys.Constant;

public class Database extends SQLiteOpenHelper {
    static Database instance = null;
    static SQLiteDatabase database = null;

    static final String DATABASE_NAME = "DB.db";
    static final int DATABASE_VERSION = 1;

    public static final String GRAPH_MAIN_TABLE = "GraphTable";
    public static final String ID = "id";
    public static final String CODE = "code";
    public static final String RECORD = "record";
    public static final String DATETIME = "datetime";


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + GRAPH_MAIN_TABLE + " ( "
                + ID + " INTEGER primary key autoincrement, "
                + CODE + " INTEGER, "
                + RECORD + " TEXT, "
                + DATETIME + " TEXT NOT NULL)");


    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + GRAPH_MAIN_TABLE);
        onCreate(db);
    }


    public static long createGraph(String code, String record, String date) {
        ContentValues cv = new ContentValues();
        cv.put(CODE, code);
        cv.put(RECORD, record);
        cv.put(DATETIME, Constant.getcuurentdate());
       // cv.put(DATETIME, date);
//        L.v("code->" + code + " data->" + date);
        return getDatabase().insert(GRAPH_MAIN_TABLE, null, cv);
    }

    public static void deletereords() {
        try {

            getDatabase().rawQuery( " DELETE FROM "+ GRAPH_MAIN_TABLE +" WHERE "+DATETIME +" < '" + Constant.getpreviousdate()+"'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<GraphModel> graphlist;

    public static ArrayList<GraphModel> getgraphdata(String name, String previousdate, String curruntdate) {
        Cursor cursor;
        try {
            graphlist = new ArrayList<GraphModel>();
            String q = "select * from " + GRAPH_MAIN_TABLE +
                    " where " + CODE + " = '" + name + "' and " + DATETIME + " BETWEEN '" + previousdate + "' AND '" + curruntdate + "' order by " + DATETIME + " DESC ";
            cursor = getDatabase().rawQuery(q, null);

            while (cursor.moveToNext()) {
                GraphModel nm = new GraphModel();
                nm.record = cursor.getString(cursor.getColumnIndex(RECORD));
                nm.datetime = cursor.getString(cursor.getColumnIndex(DATETIME));
                graphlist.add(nm);
            }
            cursor.close();
            return graphlist;
        } catch (Exception e) {
        }
        return graphlist;
    }

    Database(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static void init(Context context) {
        if (null == instance) {
            instance = new Database(context);
        }
    }

    public static SQLiteDatabase getDatabase() {
        if (null == database) {
            database = instance.getWritableDatabase();
        }
        return database;
    }

    public static void deactivate() {
        if (null != database && database.isOpen()) {
            database.close();
        }
        database = null;
        instance = null;
    }
}
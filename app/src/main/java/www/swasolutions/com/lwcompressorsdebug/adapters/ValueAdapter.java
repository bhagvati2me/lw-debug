package www.swasolutions.com.lwcompressorsdebug.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import www.swasolutions.com.lwcompressorsdebug.Pressure;
import www.swasolutions.com.lwcompressorsdebug.R;
import www.swasolutions.com.lwcompressorsdebug.listner.WidgetListner;

/**
 * Created by sebas on 22/10/2017.
 */

public class ValueAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    ArrayList<Pressure> pressures;

    public ValueAdapter(Context context, ArrayList<Pressure> pressures) {
        this.context = context;
        this.pressures = pressures;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return pressures.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        // View gridView = convertView;
        ViewHolder holder;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_mainvalue, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(pressures.get(position).name + "");



        return convertView;

    }

    private class ViewHolder {
        TextView  name;
    }

}

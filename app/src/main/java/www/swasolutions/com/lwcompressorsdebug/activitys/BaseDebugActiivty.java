package www.swasolutions.com.lwcompressorsdebug.activitys;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import www.swasolutions.com.lwcompressorsdebug.Bluetooth.L;
import www.swasolutions.com.lwcompressorsdebug.R;
import www.swasolutions.com.lwcompressorsdebug.Pressure;
import www.swasolutions.com.lwcompressorsdebug.adapters.ValueAdapter;

public class BaseDebugActiivty extends MasterBaseActivity implements OnClickListener {
    public static String CONNECTIONb = "";
    public static String deviceNameb = "";
    public static int AktVaNr = 1;
    TextView all;
    TextView date;
    TextView delay;
	//added by CAC
    TextView hDescription;
    AlertDialog delayDialog;
    TextView devicename;
    ImageView downarrow;
    ImageView enter;
    final Handler handler = new Handler();
    boolean isStreaming = false;
    ExpandableGridView maingrid;
    TextView model;
    TextView opetional,connect;
    ArrayList<Pressure> plist = new ArrayList();
    SharedPreferences pref;
    int selectedpos = -1;
    TextView tabname;
    private BroadcastReceiver the_receiver = new C02946();
    ImageView uparrow;
    ValueAdapter valueAdapter;
    Vibrator vibr;
    AlertDialog writealertDialog;
    EditText data;
    TextView Send;
    static int okcnt = 0;
    static int aufr = 0;
    static int ta0 = 0;
    String TelegramString="";

    class C02891 implements OnItemClickListener {
        C02891() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            BaseDebugActiivty.this.vibr.vibrate(50);
            BaseDebugActiivty.this.selectedpos = i;
        }
    }

    class C02924 implements OnClickListener {
        C02924() {
        }

        public void onClick(View view) {
            BaseDebugActiivty.this.delayDialog.dismiss();
        }
    }

    class C02935 implements Runnable {
        C02935() {
        }

        public void run() {
            BaseDebugActiivty.this.sendCommand(BaseDebugActiivty.this.pref.getString(Constant.data, ""),Integer.valueOf(pref.getString(Constant.values, "0")));
            Toast.makeText(BaseDebugActiivty.this, "Send data", Toast.LENGTH_SHORT).show();
            Editor editor = BaseDebugActiivty.this.pref.edit();
            editor.putString(Constant.data, "");
            editor.commit();
        }
    }



    class C02946 extends BroadcastReceiver {
        C02946() {
        }

        public void onReceive(Context c, Intent intent) {
            String[] separated;
            String DebugString;
            char DebugChar;
            int i;
            int findstart;

            String readMessage = intent.getStringExtra("msg");
            TelegramString = TelegramString + readMessage;
            findstart = TelegramString.indexOf("ST0");

            aufr ++;
            if (readMessage.contains("OK")) {
                okcnt++;
            }
            if (readMessage.contains("NOK")) {
                okcnt--;
            }

            // comment CAC: TODO read length of string and calc CRC16 of string!
            if (TelegramString.length() >= 1196) {
                BaseDebugActiivty.this.opetional.setText("");
                separated = TelegramString.split("\n");
                for (i = 0; i < separated.length; i++) {
                    if (i == 0) {
                        BaseDebugActiivty.this.devicename.setText("S" + separated[i] + "");
                    }
                    if (i == 1) {
                        BaseDebugActiivty.this.opetional.append("I/O:" + separated[i] + " ");
                    }
                    if (i == 2) {
                        BaseDebugActiivty.this.opetional.append("Mode:" + separated[i] + " ");
                    }
                    if (i == 3) {
                        BaseDebugActiivty.this.opetional.append("Status:" + separated[i] + " ");
                    }
                    if (i == 4) {
                        BaseDebugActiivty.this.opetional.append("Error:" + separated[i] + " ");
                    }
                    if (i == 5) {
                        BaseDebugActiivty.this.opetional.append("Instruction:" + separated[i] + " ");
                    }
                    if (i == 6) {
                        BaseDebugActiivty.this.opetional.append("Option:" + separated[i]);
                  
 				    }
					//added by CAC
                    if (i == 7) {
                        BaseDebugActiivty.this.hDescription.setText(Integer.toString(AktVaNr) + ": " + separated[i] + "  (OK:" + Integer.toString(okcnt) + ")");
                    }
                }
                if (TelegramString.contains("#")) {
                    separated = TelegramString.split("\n");
                    for (i = 0; i < separated.length; i++) {
                        if (separated[i].contains("#")) {
                            try {
                                if (separated[i].length() > 31) {
                                    String cutline = separated[i].substring(11);
                                    String code = cutline.substring(0, 5).replace("+", "");

                                    for (int j = 0; j < plist.size(); j++) {
                                        if (plist.get(j).code.equals(code)) {
                                            plist.get(j).name = cutline;
                                            plist.get(j).min = separated[i];
                                            //plist.get(j).code = separated[i];
                                            valueAdapter.notifyDataSetChanged();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
            // comment CAC: New TelegramString if it contains "ST0"
            if (findstart>=0)
                TelegramString = TelegramString.substring(findstart+1);

        }
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basedebug);
        this.pref = PreferenceManager.getDefaultSharedPreferences(this);
        this.vibr = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        data =(EditText)findViewById(R.id.data);
        Send =(TextView)findViewById(R.id.Send);


        this.delay = (TextView) findViewById(R.id.delay);
        this.delay.setOnClickListener(this);
        this.enter = (ImageView) findViewById(R.id.enter);
        this.enter.setOnClickListener(this);
        this.all = (TextView) findViewById(R.id.all);
        this.all.setOnClickListener(this);
        this.uparrow = (ImageView) findViewById(R.id.uparrow);
        this.downarrow = (ImageView) findViewById(R.id.downarrow);
        this.uparrow.setOnClickListener(this);
        this.downarrow.setOnClickListener(this);
        this.opetional = (TextView) findViewById(R.id.opetional);
		//added by CAC
        this.hDescription = (TextView) findViewById(R.id.hDescription);
        this.devicename = (TextView) findViewById(R.id.devicename);
        this.tabname = (TextView) findViewById(R.id.tabname);
        this.date = (TextView) findViewById(R.id.date);
        connect = (TextView) findViewById(R.id.connect);
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDeviceListActivity();
            }
        });

        Calendar c = Calendar.getInstance();
        this.date.setText(new SimpleDateFormat("dd.MM.yyyy").format(c.getTime()));
        this.model = (TextView) findViewById(R.id.model);
        this.maingrid = (ExpandableGridView) findViewById(R.id.maingrid);
        this.maingrid.setChoiceMode(1);
        this.valueAdapter = new ValueAdapter(this, this.plist);
        this.maingrid.setAdapter(this.valueAdapter);
        this.maingrid.setExpanded(true);
        setnamemodel();
        this.maingrid.setOnItemClickListener(new C02891());
        ArrayList<Pressure> list = Constant.gettotal(BaseDebugActiivty.this);
        for (int i = 0; i < list.size(); i++) {
            Pressure pobj = new Pressure(list.get(i).code, list.get(i).code,list.get(i).value);
            plist.add(pobj);

        }
        valueAdapter.notifyDataSetChanged();

        Send.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                sendCommand(BaseDebugActiivty.this. data.getText().toString(),1);
                Toast.makeText(BaseDebugActiivty.this, "Send data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showoverlay(final Pressure pobj) {
        Builder builder = new Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.dialog_all, null);
        builder.setView(mView);
        if (this.writealertDialog != null) {
            this.writealertDialog = null;
        }
        this.writealertDialog = builder.create();
        this.writealertDialog.getWindow().setSoftInputMode(4);
        final EditText editval = (EditText) mView.findViewById(R.id.editval);
        editval.setText(pobj.min + "");
        ((TextView) mView.findViewById(R.id.change)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Editor editor = BaseDebugActiivty.this.pref.edit();
                editor.putString(Constant.data, editval.getText().toString());
                L.e("value key "+pobj.value);
                editor.putString(Constant.values, pobj.value);
                editor.commit();


                BaseDebugActiivty.this.writealertDialog.dismiss();
            }
        });
        this.writealertDialog.show();
    }

    private void showdelay() {
        Builder builder = new Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.dialog_delay, null);
        builder.setView(mView);
        if (this.delayDialog != null) {
            this.delayDialog = null;
        }
        this.delayDialog = builder.create();
        final TextView seekvalue = (TextView) mView.findViewById(R.id.seekvalue);
        SeekBar seekBar = (SeekBar) mView.findViewById(R.id.seekBar);
        seekBar.setProgress(this.pref.getInt(Constant.delay, 1));
        seekvalue.setText(this.pref.getInt(Constant.delay, 1) + " ");
        seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int vl = progress;
                if (vl == 0) {
                    vl = 1;
                }
                seekvalue.setText(vl + " ");
                Editor editor = BaseDebugActiivty.this.pref.edit();
                editor.putInt(Constant.delay, vl);
                editor.commit();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        ((TextView) mView.findViewById(R.id.change)).setOnClickListener(new C02924());
        this.delayDialog.show();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.all:
                this.vibr.vibrate(50);
                if (this.selectedpos > 0) {
                    showoverlay((Pressure) this.plist.get(this.selectedpos));
                    return;
                }
                return;
            case R.id.uparrow:
                this.vibr.vibrate(50);
                this.isStreaming = false;
				//added by CAC
                AktVaNr ++;
                if (AktVaNr > 17)
                    AktVaNr = 0;
                sendVaNr(AktVaNr);
                BaseDebugActiivty.this.hDescription.setText("SET to: "+ Integer.toString(AktVaNr));
                return;

            case R.id.downarrow:
                this.vibr.vibrate(50);
                //this.valueAdapter.notifyDataSetChanged();
                this.isStreaming = true;
				//added by CAC
                if (AktVaNr > 0)
                   AktVaNr --;
                sendVaNr(AktVaNr);
                BaseDebugActiivty.this.hDescription.setText("SET to: "+ Integer.toString(AktVaNr));
                return;

            case R.id.enter:
                this.vibr.vibrate(50);
                this.handler.postDelayed(new C02935(), (long) this.pref.getInt(Constant.delay, 1));
                return;
            case R.id.delay:
                this.vibr.vibrate(50);
                showdelay();
                return;
            default:
                return;
        }
    }

    public void setmodel() {
        super.setmodel();
        deviceNameb = deviceName;
        CONNECTIONb = CONNECTION;
        setnamemodel();
    }

    public void setnamemodel() {
        try {
            if (deviceName.isEmpty()) {
                this.model.setText(CONNECTION);
            } else {
                this.model.setText(CONNECTION + " / " + deviceName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.the_receiver);
    }

    protected void onResume() {
        super.onResume();
        registerReceiver(this.the_receiver, new IntentFilter("send"));
    }
}

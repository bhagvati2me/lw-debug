package www.swasolutions.com.lwcompressorsdebug.activitys;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import www.swasolutions.com.lwcompressorsdebug.Pressure;
import www.swasolutions.com.lwcompressorsdebug.R;

/**
 * Created by bhagvati on 17/11/17.
 */

public class Constant {

    public static String data = "data" ;
    public static String values= "value" ;
    public static String delay ="" ;
    public static boolean isdebug = false; // false for live


/*    public static String Superadmin = "super123"; //1
    public static String AdminLW = "LW123"; //2
    public static String AdminImporteur = "Imp123"; //3
    public static String User = "123789"; //4*/

    public static String address = "address";
    public static String setup = "setup";
    public static String Superadmin = "Superadmin"; //1
    public static String AdminLW = "AdminLW"; //2
    public static String AdminImporteur = "AdminImporteur"; //3
    public static String User = "User"; //4

    public static String loginuser = "loginuser";
    public static String INTENT_ACTION_LANGUAGE_CHANGED = "INTENT_ACTION_LANGUAGE_CHANGED";

    public static String language = "language";
    public static int german = 2;
    public static int english = 1;

    public static String pressureunit = "pressureunit";
    public static String mode = "mode";
    public static String tempunit = "tempunit";
    public static String timer = "timer";

    public static String widgetname1 = "widgetname1";
    public static String widgetname2 = "widgetname2";
    public static String widgetname3 = "widgetname3";
    public static String widgetname4 = "widgetname4";

    public static String widgetvalue1 = "widgetvalue1";
    public static String widgetvalue2 = "widgetvalue2";
    public static String widgetvalue3 = "widgetvalue3";
    public static String widgetvalue4 = "widgetvalue4";

    public static String widgetpos1 = "widgetpos1";
    public static String widgetpos2 = "widgetpos2";
    public static String widgetpos3 = "widgetpos3";
    public static String widgetpos4 = "widgetpos4";

    public static String getmpa(String bar) {
        double mpa = Float.valueOf(bar.replace(",", ".")) * 0.1;
        String val = String.format("%.01f", mpa).replace(".", ",");
        return val;
    }

    public static String getpsi(String bar) {
        double mpa = Float.valueOf(bar.replace(",", ".")) * 14.5038;
        String val = String.format("%.01f", mpa).replace(".", ",");
        return val;
    }

    public static String getf(String bar) {
        double mpa = (Float.valueOf(bar.replace(",", ".")) * 1.8) + 32;
        String val = String.format("%.01f", mpa).replace(".", ",");
        return val;
    }

    public static String getkelvin(String bar) {
        double mpa = Float.valueOf(bar.replace(",", ".")) + 273.15;
        String val = String.format("%.01f", mpa).replace(".", ",");
        return val;
    }

    public static boolean islogin(Context con) {

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(con);
        if (pref.getInt(loginuser, 0) != 0)
            return true;
        else
            return false;

    }

    public static int getlogincode(Context con) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(con);
        return pref.getInt(loginuser, 0);
    }

    public static void setdevice(Context con, String devicename) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(con);
        SharedPreferences.Editor ed = pref.edit();
        ed.putString(address, devicename);
        ed.commit();

    }

    public static ArrayList<Pressure> gettotal(Context context) {
        ArrayList<Pressure> pressures = new ArrayList<>();
        pressures.add(new Pressure(context.getString(R.string.pressure1ststage), "", "STA1", "0", "150"));
        pressures.add(new Pressure(context.getString(R.string.Pressure2ndStage), "", "STA2", "0", "150"));
        pressures.add(new Pressure(context.getString(R.string.Pressure3rdStage), "", "STA3", "0", "150"));
        //pressures.add(new Pressure(context.getString(R.string.Pressure4thStage), "", "", "0", "150"));//no flag
        pressures.add(new Pressure(context.getString(R.string.IntakePressure), "", "VOR0", "0", "150"));
        pressures.add(new Pressure(context.getString(R.string.OilPressure), "", "OIP0", "0", "20"));
     //   pressures.add(new Pressure(context.getString(R.string.StartPressure), "", "", "0", "2000"));//no flag
        pressures.add(new Pressure(context.getString(R.string.FinalPressure), "5", "END0", "0", "2000"));
       // pressures.add(new Pressure(context.getString(R.string.IntakePressureMin), "", "", "-0.1", "150"));//no flag
       // pressures.add(new Pressure(context.getString(R.string.IntakePressureMAx), "", "", "0", "150"));//no flag
       // pressures.add(new Pressure(context.getString(R.string.Runover), "", "", "0", "50"));//no flag
       // pressures.add(new Pressure(context.getString(R.string.Temperature1stStage), "", "", "0", "250"));//no flag
       // pressures.add(new Pressure(context.getString(R.string.Temperature2ndStage), "", "", "0", "250"));//no flag
       // pressures.add(new Pressure(context.getString(R.string.Temperature3rdStage), "", "", "0", "250"));//no flag
       // pressures.add(new Pressure(context.getString(R.string.Temperature4thStage), "", "", "0", "250"));//no flag
        pressures.add(new Pressure(context.getString(R.string.FinalTemperature), "7", "CYL0", "0", "250"));
        pressures.add(new Pressure(context.getString(R.string.OilTemperature), "", "OIT0", "0", "250"));
        pressures.add(new Pressure(context.getString(R.string.OpionalTemperature), "", "ANS0", "0", "250"));
        pressures.add(new Pressure(context.getString(R.string.Ambienttemperature), "", "SIL0", "0", "130"));
      //  pressures.add(new Pressure(context.getString(R.string.BlockHeater), "", "", "0", "25"));//no flag
      //  pressures.add(new Pressure(context.getString(R.string.IntakeCO2), "", "", "0", "150")); //no flag
        pressures.add(new Pressure(context.getString(R.string.Flow), "", "FLO0", "0", "5"));
        pressures.add(new Pressure(context.getString(R.string.AirQuality), "", "VOC1", "0", "120"));
        pressures.add(new Pressure(context.getString(R.string.Humidity), "", "HUM0", "0", "120"));
        pressures.add(new Pressure(context.getString(R.string.CO), "", "CO_0", "0", "10"));
        pressures.add(new Pressure(context.getString(R.string.CO2), "", "CO20", "0", "2000"));
        pressures.add(new Pressure(context.getString(R.string.O2), "", "O2_0", "15", "25"));
        pressures.add(new Pressure(context.getString(R.string.Oil), "", "VOC0", "0", "7"));
        pressures.add(new Pressure(context.getString(R.string.Gast), "", "GAT0", "4", "45"));

        return pressures;
    }

    public static ArrayList<Pressure> getpressure(Context context) {
        int code = Constant.getlogincode(context);
        ArrayList<Pressure> pressures = new ArrayList<>();
        pressures.add(new Pressure(context.getString(R.string.pressure1ststage), "", "STA1", "0", "150"));
        pressures.add(new Pressure(context.getString(R.string.Pressure2ndStage), "", "STA2", "0", "150"));
        pressures.add(new Pressure(context.getString(R.string.Pressure3rdStage), "", "STA3", "0", "150"));
        pressures.add(new Pressure(context.getString(R.string.Pressure4thStage), "", "", "0", "150"));//no flag
        pressures.add(new Pressure(context.getString(R.string.IntakePressure), "", "VOR0", "0", "150"));
        pressures.add(new Pressure(context.getString(R.string.OilPressure), "", "OIP0", "0", "20"));
        pressures.add(new Pressure(context.getString(R.string.StartPressure), "", "", "0", "2000"));//no flag
        pressures.add(new Pressure(context.getString(R.string.FinalPressure), "", "END0", "0", "2000"));
        //  pressures.add(new Pressure(context.getString(R.string.END), "", "END0"));

        if (code == 1 || code == 2 || code == 3) {
            pressures.add(new Pressure(context.getString(R.string.IntakePressureMin), "", "", "-0.1", "150"));//no flag
            pressures.add(new Pressure(context.getString(R.string.IntakePressureMAx), "", "", "0", "150"));//no flag
        }
        if (code == 1 || code == 2) {
            pressures.add(new Pressure(context.getString(R.string.Runover), "", "", "0", "50"));//no flag
        }

        return pressures;
    }

    public static ArrayList<Pressure> gettemp(Context context) {
        int code = Constant.getlogincode(context);
        ArrayList<Pressure> pressures = new ArrayList<>();

        if (code == 1 || code == 2 || code == 3) {
            pressures.add(new Pressure(context.getString(R.string.Temperature1stStage), "", "", "0", "250"));//no flag
            pressures.add(new Pressure(context.getString(R.string.Temperature2ndStage), "", "", "0", "250"));//no flag
            pressures.add(new Pressure(context.getString(R.string.Temperature3rdStage), "", "", "0", "250"));//no flag
            pressures.add(new Pressure(context.getString(R.string.Temperature4thStage), "", "", "0", "250"));//no flag
            pressures.add(new Pressure(context.getString(R.string.FinalTemperature), "", "CYL0", "0", "250"));
            pressures.add(new Pressure(context.getString(R.string.OilTemperature), "", "OIT0", "0", "250"));
            pressures.add(new Pressure(context.getString(R.string.OpionalTemperature), "", "ANS0", "0", "250"));
        }
        if (code == 1 || code == 2) {
            pressures.add(new Pressure(context.getString(R.string.Ambienttemperature), "", "SIL0", "0", "130"));
        }

        pressures.add(new Pressure(context.getString(R.string.BlockHeater), "", "", "0", "25"));//no flag

        return pressures;
    }

    public static ArrayList<Pressure> getair(Context context) {
        int code = Constant.getlogincode(context);
        ArrayList<Pressure> pressures = new ArrayList<>();
        pressures.add(new Pressure(context.getString(R.string.IntakeCO2), "", "", "0", "150")); //no flag
        pressures.add(new Pressure(context.getString(R.string.Flow), "", "FLO0", "0", "5"));
        pressures.add(new Pressure(context.getString(R.string.AirQuality), "", "VOC1", "0", "120"));
        pressures.add(new Pressure(context.getString(R.string.Humidity), "", "HUM0", "0", "120"));
        pressures.add(new Pressure(context.getString(R.string.CO), "", "CO_0", "0", "10"));
        pressures.add(new Pressure(context.getString(R.string.CO2), "", "CO20", "0", "2000"));
        pressures.add(new Pressure(context.getString(R.string.O2), "", "O2_0", "15", "25"));
        pressures.add(new Pressure(context.getString(R.string.Oil), "", "VOC0", "0", "7"));
        pressures.add(new Pressure(context.getString(R.string.Gast), "", "GAT0", "4", "45"));
        return pressures;
    }

    public static int getRandom(int from, int to) {
        if (from < to)
            return from + new Random().nextInt(Math.abs(to - from));
        return from - new Random().nextInt(Math.abs(to - from));
    }

    public static String getmincount(Date date1, Date date2, int type) {
        long diff = date1.getTime() - date2.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long day = hours / 24;
        if (type == 0) {
            return minutes + "";
        } else if (type == 1) {
            return hours + "";
        } else {
            return day + "";
        }
    }

    public static Date convertToDate(String orgdate) {
        try {
            Date date = dateFormat.parse(orgdate);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

    }

    static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());

    public static String getcuurentdate() {

        Calendar calendar = Calendar.getInstance();
        String yesterdayAsString = dateFormat.format(calendar.getTime());
        return yesterdayAsString;
    }

    public static String getpreviousdate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -96);
        String yesterdayAsString = dateFormat.format(calendar.getTime());
        return yesterdayAsString;
    }


    public static ArrayList<Pressure> getwidget(Context context) {
        ArrayList<Pressure> pressures = new ArrayList<>();
        pressures.addAll(getpressure(context));
        pressures.addAll(gettemp(context));
        pressures.addAll(getair(context));
        return pressures;

    }

    public static String getrefine(String code) {

        String val = code;

        if (code.contains("bar")) {
            String c = code.replace("bar", "").trim();
            double mpa = Float.valueOf(c.replace(",", "."));
            val = String.format("%.01f", mpa);
            val += " bar";
            return val;
        } else if (code.contains("GradC")) {
            String c = code.replace("GradC", "").trim();
            double mpa = Float.valueOf(c.replace(",", "."));
            val = String.format("%.01f", mpa);
            val += " GradC";
            return val;
        } else if (code.contains("mg/m3")) {
            String c = code.replace("mg/m3", "").trim();
            double mpa = Float.valueOf(c.replace(",", "."));
            val = String.format("%.01f", mpa);
            val += " mg/m3";
            return val;
        } else if (code.contains("ppm")) {
            String c = code.replace("ppm", "").trim();
            double mpa = Float.valueOf(c.replace(",", "."));
            val = String.format("%.01f", mpa);
            val += " ppm";
            return val;
        } else if (code.contains("%")) {
            String c = code.replace("%", "").trim();
            double mpa = Float.valueOf(c.replace(",", "."));
            val = String.format("%.01f", mpa);
            val += " %";
            return val;
        } else if (code.contains("l/min")) {
            String c = code.replace("l/min", "").trim();
            double mpa = Float.valueOf(c.replace(",", "."));
            val = String.format("%.01f", mpa);
            val += " l/min";
            return val;
        }
        return val;


    }

    public static String getNamefromcode(String code) {
        if (code.equals("STA1")) {
            return "Pressure 1st Stage";
        } else if (code.equals("STA2")) {
            return "Pressure 2nd Stage";
        } else if (code.equals("STA3")) {
            return "Pressure 3rd Stage";
        } else if (code.equals("VOR0")) {
            return "Intake Pressure";
        } else if (code.equals("VOC1")) {
            return "Air Quality";
        } else if (code.equals("END0")) {
            return "END";
        } else if (code.equals("OIP0")) {
            return "Oil Pressure";
        } else if (code.equals("HUM1")) {
            return "Final Pressure";
        } else if (code.equals("CYL0")) {
            return "Final Temperature";
        } else if (code.equals("OIT0")) {
            return "Oil Temperature";
        } else if (code.equals("SIL0")) {
            return "Ambienttemperature";
        } else if (code.equals("ANS0")) {
            return "Opional Temperature";
        } else if (code.equals("HUM0")) {
            return "Humidity";
        } else if (code.equals("CO_0")) {
            return "CO";
        } else if (code.equals("CO20")) {
            return "CO2";
        } else if (code.equals("O2_0")) {
            return "O2";
        } else if (code.equals("VOC0")) {
            return "Oil";
        } else if (code.equals("VOC1")) {
            return "Air Quality";
        } else if (code.equals("GAT0")) {
            return "Gas (t)";
        }
        return "";
    }
}

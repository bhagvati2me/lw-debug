package www.swasolutions.com.lwcompressorsdebug.activitys;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import www.swasolutions.com.lwcompressorsdebug.R;

public class SplashScreenActivity extends AppCompatActivity {

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        ThreadTask threadTask = new ThreadTask();
        threadTask.execute();


//        String data = "#500000210 ANS0+     0,0  GradC ";
//        data = data + crc(data.getBytes());
//        L.e("@@  " + data);

    }


   /* public static int CRC16(int crc, int data)
    {
        int Poly16 = 0xA001;
        int LSB, i;
        crc = ((crc ^ data) | 0xFF00) & (crc | 0x00FF);
        for (i = 0; i < 8; i++) {
            LSB = (crc & 0x0001);
            crc = crc / 2;
            L.e("LSB " + LSB);
            if (LSB == 1)
                crc = crc ^ Poly16;
        }
        return (crc);

    }

    public static String crc(byte[] args) {
        int crc = 0xFFFF;
        byte[] bytes = args;
        for (byte b : bytes) {
            crc = CRC16(crc, b);
        }
        String le = Integer.toHexString(crc);
        le = le + "\\r\\n";
        return le;
    }*/


    /**
     * Method that will stop the thread for one second.
     */
    private void second() {
        try {

            Thread.sleep(200); //stop thread for one second = 1000 millisecond

        } catch (InterruptedException e) {

        }
    }

    /**
     * @author This class implements AsyncTask, is the most appropriate
     *         way to use threads in android
     */

    private class ThreadTask extends AsyncTask<Void, Integer, Boolean> {


        public ThreadTask() {
            super();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setMax(100);
            progressBar.setProgress(0);
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            for (int i = 1; i <= 5; i++) {
                second();
                publishProgress(i * 20);

                if (isCancelled()) {
                    break;
                }
            }

            return true;
        }


        @Override
        protected void onPostExecute(Boolean booleana) {
            super.onPostExecute(booleana);

            finish();
            Intent intent = new Intent(getApplicationContext(), BaseDebugActiivty.class);
            startActivity(intent);


        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0].intValue());

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

            Toast.makeText(getApplicationContext(), "Cancel", Toast.LENGTH_SHORT).show();
        }
    }

}

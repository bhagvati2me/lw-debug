package www.swasolutions.com.lwcompressorsdebug;

/**
 * Created by sebas on 22/10/2017.
 */

public class Pressure {

    public String name;
    public String value;
    public String code;
    public String min;
    public String max;

    public Pressure( String name, String code,String s) {
        this.name = name;
        this.code = code;
        this.value = s;
    }

    public Pressure(String name, String value, String code, String min, String max) {
        this.name = name;
        this.value = value;
        this.code = code;
        this.min = min;
        this.max = max;

    }


}

package www.swasolutions.com.lwcompressorsdebug.Bluetooth;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.BuildConfig;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;

/**
 * Ð’Ñ�Ð¿Ð¾Ð¼Ð¾Ð³Ð°Ñ‚ÐµÐ»ÑŒÐ½Ñ‹Ðµ Ð¼ÐµÑ‚Ð¾Ð´Ñ‹ Created by sash0k on 29.01.14.
 */
public class Utils {

	static final String TAG = "SPP_TERMINAL";

	/**
	 * ÐžÐ±Ñ‰Ð¸Ð¹ Ð¼ÐµÑ‚Ð¾Ð´ Ð²Ñ‹Ð²Ð¾Ð´Ð° Ð¾Ñ‚Ð»Ð°Ð´Ð¾Ñ‡Ð½Ñ‹Ñ…
	 * Ñ�Ð¾Ð¾Ð±Ñ‰ÐµÐ½Ð¸Ð¹ Ð² Ð»Ð¾Ð³
	 */
	public static void log(String message) {
		if (BuildConfig.DEBUG) {
			if (message != null)
				Log.i(TAG, message);
		}
	}
	// ============================================================================

	/**
	 * ÐšÐ¾Ð½Ð²ÐµÑ€Ñ‚Ð°Ñ†Ð¸Ñ� hex-ÐºÐ¾Ð¼Ð°Ð½Ð´ Ð² Ñ�Ñ‚Ñ€Ð¾ÐºÑƒ Ð´Ð»Ñ�
	 * Ð¾Ñ‚Ð¾Ð±Ñ€Ð°Ð¶ÐµÐ½Ð¸Ñ�
	 */
	public static String printHex(String hex) {
		StringBuilder sb = new StringBuilder();
		int len = hex.length();
		try {
			for (int i = 0; i < len; i += 2) {
				sb.append("0x").append(hex.substring(i, i + 2)).append(" ");
			}
		} catch (NumberFormatException e) {
			log("printHex NumberFormatException: " + e.getMessage());

		} catch (StringIndexOutOfBoundsException e) {
			log("printHex StringIndexOutOfBoundsException: " + e.getMessage());
		}
		return sb.toString();
	}
	// ============================================================================

	/**
	 * ÐŸÐµÑ€ÐµÐ²Ð¾Ð´ Ð²Ð²ÐµÐ´ÐµÐ½Ð½Ñ‹Ñ… ASCII-ÐºÐ¾Ð¼Ð°Ð½Ð´ Ð² hex
	 * Ð¿Ð¾Ð±Ð°Ð¹Ñ‚Ð½Ð¾.
	 * 
	 * @param hex
	 *            - ÐºÐ¾Ð¼Ð°Ð½Ð´Ð°
	 * @return - Ð¼Ð°Ñ�Ñ�Ð¸Ð² Ð±Ð°Ð¹Ñ‚ ÐºÐ¾Ð¼Ð°Ð½Ð´Ñ‹
	 */
	public static byte[] toHex(String hex) {
		int len = hex.length();
		byte[] result = new byte[len];
		try {
			int index = 0;
			for (int i = 0; i < len; i += 2) {
				result[index] = (byte) Integer.parseInt(hex.substring(i, i + 2), 16);
				index++;
			}
		} catch (NumberFormatException e) {
			log("toHex NumberFormatException: " + e.getMessage());

		} catch (StringIndexOutOfBoundsException e) {
			log("toHex StringIndexOutOfBoundsException: " + e.getMessage());
		}
		return result;
	}
	// ============================================================================

	/**
	 * ÐœÐµÑ‚Ð¾Ð´ Ñ�Ð»Ð¸Ð²Ð°ÐµÑ‚ Ð´Ð²Ð° Ð¼Ð°Ñ�Ñ�Ð¸Ð²Ð° Ð² Ð¾Ð´Ð¸Ð½
	 */
	public static byte[] concat(byte[] A, byte[] B) {
		byte[] C = new byte[A.length + B.length];
		System.arraycopy(A, 0, C, 0, A.length);
		System.arraycopy(B, 0, C, A.length, B.length);
		return C;
	}
	// ============================================================================

	/**
	 * ÐŸÐ¾Ð»ÑƒÑ‡ÐµÐ½Ð¸Ðµ id Ñ�Ð¾Ñ…Ñ€Ð°Ð½Ñ‘Ð½Ð½Ð¾Ð³Ð¾ Ð² Ð¸Ð³Ñ€ÑƒÑˆÐºÐµ
	 * Ð·Ð²ÑƒÐºÐ¾Ð²Ð¾Ð³Ð¾ Ð½Ð°Ð±Ð¾Ñ€Ð°
	 */
	public static String getPrefence(Context context, String item) {
		final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
		return settings.getString(item, TAG);
	}
	// ============================================================================

	/**
	 * ÐŸÐ¾Ð»ÑƒÑ‡ÐµÐ½Ð¸Ðµ Ñ„Ð»Ð°Ð³Ð° Ð¸Ð· Ð½Ð°Ñ�Ñ‚Ñ€Ð¾ÐµÐº
	 */
	public static boolean getBooleanPrefence(Context context, String tag) {
		final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
		return settings.getBoolean(tag, true);
	}
	// ============================================================================

	/**
	 * ÐšÐ»Ð°Ñ�Ñ�-Ñ„Ð¸Ð»ÑŒÑ‚Ñ€ Ð¿Ð¾Ð»ÐµÐ¹ Ð²Ð²Ð¾Ð´Ð°
	 */
	// ============================================================================
	public static class InputFilterHex implements InputFilter {

		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			for (int i = start; i < end; i++) {
				if (!Character.isDigit(source.charAt(i)) && source.charAt(i) != 'A' && source.charAt(i) != 'D'
						&& source.charAt(i) != 'B' && source.charAt(i) != 'E' && source.charAt(i) != 'C'
						&& source.charAt(i) != 'F') {
					return "";
				}
			}
			return null;
		}
	}
	// ============================================================================
}

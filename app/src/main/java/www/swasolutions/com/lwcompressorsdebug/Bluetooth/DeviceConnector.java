package www.swasolutions.com.lwcompressorsdebug.Bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import www.swasolutions.com.lwcompressorsdebug.activitys.BaseDebugActiivty;

public class DeviceConnector {
    private static final String TAG = "DeviceConnector";
    private static final boolean D = false;

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0; // we're doing nothing
    public static final int STATE_CONNECTING = 1; // now initiating an outgoing
    // connection
    public static final int STATE_CONNECTED = 2; // now connected to a remote
    // device
    public static String DEVICEADDRESS = "";
    private int mState;

    private final BluetoothAdapter btAdapter;
    private final BluetoothDevice connectedDevice;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private final Handler mHandler;
    private final String deviceName;

    // ==========================================================================

    public DeviceConnector(DeviceData deviceData, Handler handler) {
        mHandler = handler;
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        connectedDevice = btAdapter.getRemoteDevice(deviceData.getAddress());
        deviceName = (deviceData.getName() == null) ? deviceData.getAddress()
                : deviceData.getName();
        mState = STATE_NONE;
    }

    // ==========================================================================

    /**
     * Запро�? на �?оединение �? у�?той�?твом
     */
    public synchronized void connect() {
        if (D)
            Log.d(TAG, "connect to: " + connectedDevice);

        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {
                if (D)
                    Log.d(TAG, "cancel mConnectThread");
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        if (mConnectedThread != null) {
            if (D)
                Log.d(TAG, "cancel mConnectedThread");
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(connectedDevice);
        mConnectThread.start();
        setState(STATE_CONNECTING);
        DEVICEADDRESS = connectedDevice.getAddress();


    }

    // ==========================================================================

    /**
     * Завершение �?оединени�?
     */
    public synchronized void stop() {
        if (D)
            Log.d(TAG, "stop");

        if (mConnectThread != null) {
            if (D)
                Log.d(TAG, "cancel mConnectThread");
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            if (D)
                Log.d(TAG, "cancel mConnectedThread");
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(STATE_NONE);
    }

    // ==========================================================================

    /**
     * У�?тановка внутреннего �?о�?то�?ни�? у�?трой�?тва
     *
     * @param state - �?о�?то�?ние
     */
    private synchronized void setState(int state) {
        if (D)
            Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;
        mHandler.obtainMessage(BaseDebugActiivty.MESSAGE_STATE_CHANGE, state, -1)
                .sendToTarget();
    }

    // ==========================================================================

    /**
     * Получение �?о�?то�?ни�? у�?трой�?тва
     */
    public synchronized int getState() {
        return mState;
    }

    // ==========================================================================

    public synchronized void connected(BluetoothSocket socket) {
        if (D)
            Log.d(TAG, "connected");

        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            if (D)
                Log.d(TAG, "cancel mConnectThread");
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            if (D)
                Log.d(TAG, "cancel mConnectedThread");
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(STATE_CONNECTED);

        // Send the name of the connected device back to the UI Activity
        Message msg = mHandler.obtainMessage(BaseDebugActiivty.MESSAGE_DEVICE_NAME,
                deviceName);
        mHandler.sendMessage(msg);

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();
    }

    // ==========================================================================

    public void write(byte[] data) {
        ConnectedThread r;

        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED)
                return;
            r = mConnectedThread;
        }
        // Perform the write unsynchronized
        if (data.length == 1)
            r.write(data[0]);
        else {
            r.writeData(data);
            L.e("write data: " + (data));
        }
    }

    private static byte[] getBytes(int v) {
        byte[] writeBuffer = new byte[4];
        writeBuffer[3] = (byte) ((v >>> 24) & 0xFF);
        writeBuffer[2] = (byte) ((v >>> 16) & 0xFF);
        writeBuffer[1] = (byte) ((v >>> 8) & 0xFF);
        writeBuffer[0] = (byte) ((v >>> 0) & 0xFF);
        return writeBuffer;
    }

    private byte[] crc16(final byte[] buffer) {

        int crc = 0x1D0F;
        for (int j = 0; j < buffer.length; j++) {
            crc = ((crc >>> 8) | (crc << 8)) & 0xffff;
            crc ^= (buffer[j] & 0xff);//byte to int, trunc sign
            crc ^= ((crc & 0xff) >> 4);
            crc ^= (crc << 12) & 0xffff;
            crc ^= ((crc & 0xFF) << 5) & 0xffff;
        }
        crc &= 0xffff;

        byte[] returnBytes = new byte[2];
        byte[] crcBytes = getBytes(crc);
        // The first two bytes of crcBytes are low and high bytes respectively.
        for (int i = 0; i < returnBytes.length; i++) {
            returnBytes[i] = crcBytes[i];
        }

        ByteBuffer bb = ByteBuffer.allocate(buffer.length + returnBytes.length);
        bb.put(buffer);
        bb.put(returnBytes);
        byte[] result = bb.array();

        return result;
    }

    // ==========================================================================

    private void connectionFailed() {
        if (D)
            Log.d(TAG, "connectionFailed");

        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(BaseDebugActiivty.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        msg.setData(bundle);
        mHandler.sendMessage(msg);
        setState(STATE_NONE);
    }

    // ==========================================================================

    private void connectionLost() {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(BaseDebugActiivty.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        msg.setData(bundle);
        mHandler.sendMessage(msg);
        setState(STATE_NONE);
    }

    // ==========================================================================

    /**
     * Кла�?�? потока дл�? �?оединени�? �? BT-у�?трой�?твом
     */
    // ==========================================================================
    private class ConnectThread extends Thread {
        private static final String TAG = "ConnectThread";
        private static final boolean D = false;

        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            if (D)
                Log.d(TAG, "create ConnectThread");
            mmDevice = device;
            mmSocket = BluetoothUtils.createRfcommSocket(mmDevice);
        }

        // ==========================================================================

        /**
         * О�?новной рабочий метод дл�? �?оединени�? �? у�?трой�?твом. При у�?пешном
         * �?оединении передаёт управление другому потоку
         */
        public void run() {
            if (D)
                Log.d(TAG, "ConnectThread run");
            btAdapter.cancelDiscovery();
            if (mmSocket == null) {
                if (D)
                    Log.d(TAG,
                            "unable to connect to device, socket isn't created");
                connectionFailed();
                return;
            }

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                mmSocket.connect();
            } catch (IOException e) {
                // Close the socket
                try {
                    mmSocket.close();
                } catch (IOException e2) {
                    if (D)
                        Log.e(TAG,
                                "unable to close() socket during connection failure",
                                e2);
                }
                connectionFailed();
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (DeviceConnector.this) {
                mConnectThread = null;
            }

            // Start the connected thread
            connected(mmSocket);
        }

        // ==========================================================================

        /**
         * Отмена �?оединени�?
         */
        public void cancel() {
            if (D)
                Log.d(TAG, "ConnectThread cancel");

            if (mmSocket == null) {
                if (D)
                    Log.d(TAG, "unable to close null socket");
                return;
            }
            try {
                mmSocket.close();
            } catch (IOException e) {
                if (D)
                    Log.e(TAG, "close() of connect socket failed", e);
            }
        }
        // ==========================================================================
    }

    // ==========================================================================

    /**
     * Кла�?�? потока дл�? обмена данными �? BT-у�?трой�?твом
     */
    // ==========================================================================
    private class ConnectedThread extends Thread {
        private static final String TAG = "ConnectedThread";
        private static final boolean D = false;

        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            if (D)
                Log.d(TAG, "create ConnectedThread");

            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                if (D)
                    Log.e(TAG, "temp sockets not created", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        // ==========================================================================

        /**
         * О�?новной рабочий метод - ждёт вход�?щих команд от потока
         */
        public void run() {
            if (D)
                Log.i(TAG, "ConnectedThread run");
            byte[] buffer = new byte[512];
            int bytes;
            StringBuilder readMessage = new StringBuilder();
            while (true) {
                try {
                    // �?читываю вход�?щие данные из потока и �?обираю в �?троку
                    // ответа
                    bytes = mmInStream.read(buffer);
                    String readed = new String(buffer, 0, bytes);
                    readMessage.append(readed);

                    // маркер конца команды - вернуть ответ в главный поток
                    if (readed.contains("\n")) {

                        L.e("From BT -> " + readMessage.toString());
                        mHandler.obtainMessage(BaseDebugActiivty.MESSAGE_READ,
                                bytes, -1, readMessage.toString())
                                .sendToTarget();
                        readMessage.setLength(0);
                    }

                } catch (IOException e) {
                    if (D)
                        Log.e(TAG, "disconnected", e);
                    connectionLost();
                    break;
                }
            }
        }

        // ==========================================================================

        /**
         * Запи�?ать ку�?ок данных в у�?трой�?тво
         */
        public void writeData(byte[] chunk) {

            try {
                mmOutStream.write(chunk);
                mmOutStream.flush();
                mHandler.obtainMessage(BaseDebugActiivty.MESSAGE_WRITE, -1, -1,
                        chunk).sendToTarget();
            } catch (IOException e) {
                if (D)
                    Log.e(TAG, "Exception during write", e);
            }
        }

        // ==========================================================================

        /**
         * Запи�?ать байт
         */
        public void write(byte command) {
            byte[] buffer = new byte[1];
            buffer[0] = command;

            try {
                mmOutStream.write(buffer);


                // Share the sent message back to the UI Activity
                mHandler.obtainMessage(BaseDebugActiivty.MESSAGE_WRITE, -1, -1,
                        buffer).sendToTarget();
            } catch (IOException e) {
                if (D)
                    Log.e(TAG, "Exception during write", e);
            }
        }

        // ==========================================================================

        /**
         * Отмена - закрытие �?окета
         */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                if (D)
                    Log.e(TAG, "close() of connect socket failed", e);
            }
        }
        // ==========================================================================
    }
    // ==========================================================================
}
